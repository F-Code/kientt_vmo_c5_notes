package com.example.demo.models;

/**
 * Role enum
 * Arthur: kientt
 */
public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN;
}
